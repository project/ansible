<?php

namespace Drupal\ansible\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ansible entity entities.
 *
 * @ingroup ansible
 */
class AnsibleEntityDeleteForm extends ContentEntityDeleteForm {


}
